/**
 * authors:			Max Braungardt & Hannes Thalheim
 */

#include "DHT11.h"

DHT11::DHT11(int pin) {
  this->pin = pin;

  // Bus idle state
  pinMode( pin, OUTPUT );
  digitalWrite( pin, HIGH );
}

bool DHT11::readSensor(void) {
  unsigned long errWaitTime = 50; // ms
  auto idleState = [this]() {
    pinMode( this->pin, OUTPUT );
    digitalWrite( this->pin, HIGH );
  };

  // Initial pulse from Host, then listening on pin
  pinMode( pin, OUTPUT );
  digitalWrite( pin, LOW );
  delayMicroseconds( initHostTime );
  digitalWrite( pin, HIGH );
  pinMode( pin, INPUT_PULLUP );

  auto pulseDuration = [this](int state) {
    unsigned long start = 0;

    // Abort if initial state is wrong
    if ( digitalRead( this->pin ) != state ) {
      return start;
    } else {
      while ( digitalRead( this->pin ) == state );

      start = micros();
      while ( digitalRead(this->pin) != state);
      return micros() - start;
    }
  };

  // Handshake Host <-> Slave
  while ( digitalRead(pin) == HIGH );
  if ( !pulseDuration(LOW) ) {
    Serial.println( "Handshake Error" );
    delay( errWaitTime );
    idleState();
    return 0;
  }

  // Read data bits
  unsigned long duration = 0;
  for ( int i = 0; i < 40; i++ ) {
    duration = pulseDuration( LOW );
    if ( !duration ) {
      Serial.println( "Bit " + String(i) + " Read Error " + String(duration) );
      delay( errWaitTime );
      idleState();
      return 0;
    }
    data[i / 8] <<= 1;
    data[i / 8] |= ( duration > initBitsTime ? 1 : 0 );
  }

  // Check transmitted data
  if ( data[4] != ( data[0] + data[1] + data[2] + data[3] ) ) {
    Serial.println( "Checksum Error" );
    delay( errWaitTime );
    idleState();
    return 0;
  }

  // Finished Transmission - wait for HIGH
  while ( digitalRead(pin) == LOW );

  idleState();

  return 1;
}

double DHT11::getTemperature() {
  return bytesToDouble( data[2], data[3] );
}

double DHT11::getHumidity() {
  return bytesToDouble( data[0], data[1] );
}

double DHT11::bytesToDouble(byte b1, byte b2) {
  double result = b1;
  int n = -1;

  for( byte bit = 128; bit > 0; bit >>= 1) {
    result += ( pow(2, n--) * (b2 & bit) );
  }

  return result;
}
