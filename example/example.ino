/**
 * authors:       Max Braungardt & Hannes Thalheim
 */

#include <DHT11.h>

const int pin = 5;
const unsigned long cooldown = 5000;
unsigned long lastCycle = 0;

DHT11 dht = DHT11( pin );

void setup() {
  Serial.begin( 9600 );
}

void loop() {
  unsigned long now = millis();
  if( now - lastCycle > cooldown ) {
    dht.readSensor();
    Serial.println( "Temperatur:\t\t" + String(dht.getTemperature()) + " °C" );
    Serial.println( "Luftfeuchtigkeit:\t" + String(dht.getHumidity()) + " %" );
    lastCycle = now;
  }
}

