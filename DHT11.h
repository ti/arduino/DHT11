/**
 * authors:			Max Braungardt & Hannes Thalheim
 */

#ifndef DHT11_HPP
#define DHT11_HPP

#include "Arduino.h"

class DHT11 {
  public:
      /**
       * Constructor
       */
      DHT11(int);
      /**
       * Reads all data provided by the DHT11 and stores it
       * on the arduino in a private byte-array for further processing.
       */
      bool readSensor(void);
      double getTemperature(void);                // in °C
      double getHumidity(void);                   // in %

  private:
      /**
       * The pin which is used for bidirectional communication
       * between the arduino and the DHT11.
       */
      int pin;
      byte data[5];
      const unsigned long initHostTime = 30000;   // in µs
      const unsigned long initSlavTime = 80;      // in µs
      const unsigned long initBitsTime = 50;      // in µs
      /**
       * Takes two bytes and converts them into a double value.
       * (where the first byte is the integer value
       * and the second byte is the floating value between 0 and 1)
       */
      double bytesToDouble( byte, byte );         // take two bytes and make a double
};

#endif
